#!/bin/bash

set -eux

TS3SERVER_LICENSE=accept

mkdir -p /app/data/ts/files /run/nginx/log /run/nginx/lib

if [[ ! -d /app/data/ts/ts3server.ini ]]; then
    echo "=> Detected first run"
    #cp /app/code/teamspeak3-server_linux_amd64/redist/libmariadb.so.2 /app/code/teamspeak3-server_linux_amd64

# create ts3server.ini file
echo "
machine_id=
default_voice_port=${DEFAULT_PORT}
voice_ip=0.0.0.0
licensepath=
filetransfer_port=${FILETRANSFER_PORT}
filetransfer_ip=0.0.0.0
query_port=${SERVERQUERY_PORT}
query_ip=0.0.0.0
dbplugin=ts3db_mariadb
dbpluginparameter=ts3db_mysql.ini
dbsqlpath=sql/
dbsqlcreatepath=create_mariadb/
logpath=/run
logquerycommands=0
query_ip_whitelist=/app/data/ts/query_ip_whitelist.txt
query_ip_blacklist=/app/data/ts/query_ip_blacklist.txt
query_protocols=raw
query_ssh_rsa_host_key=ssh_host_rsa_key
" > /app/data/ts/ts3server.ini

# create mysql config file
echo "
[config]
host='${MYSQL_HOST}'
port='3306'
username='${MYSQL_USERNAME}'
password='${MYSQL_PASSWORD}'
database='${MYSQL_DATABASE}'
socket=
" > /app/data/ts/ts3db_mysql.ini

touch /app/data/ts/query_ip_whitelist.txt
touch /app/data/ts/query_ip_blacklist.txt

fi


# update mysql information
sed -i "s/host=.*/host='${MYSQL_HOST}'/" /app/data/ts/ts3db_mysql.ini
sed -i "s/username=.*/username='${MYSQL_USERNAME}'/" /app/data/ts/ts3db_mysql.ini
sed -i "s/password=.*/password='${MYSQL_PASSWORD}'/" /app/data/ts/ts3db_mysql.ini
sed -i "s/database=.*/database='${MYSQL_DATABASE}'/" /app/data/ts/ts3db_mysql.ini

# get/set ports
if [ -z ${DEFAULT_PORT+x} ]; then DEFAULT_PORT="8801"; else echo "DEFAULT_PORT is set to '$DEFAULT_PORT'"; fi
if [ -z ${SERVERQUERY_PORT+x} ]; then SERVERQUERY_PORT="10011"; else echo "SERVERQUERY_PORT is set to '$SERVERQUERY_PORT'"; fi
if [ -z ${FILETRANSFER_PORT+x} ]; then FILETRANSFER_PORT="30033"; else echo "FILETRANSFER_PORT is set to '$FILETRANSFER_PORT'"; fi

sed -i "s/default_voice_port=.*/default_voice_port=${DEFAULT_PORT}/" /app/data/ts/ts3server.ini
sed -i "s/filetransfer_port=.*/filetransfer_port=${FILETRANSFER_PORT}/" /app/data/ts/ts3server.ini
sed -i "s/query_port=.*/query_port=${SERVERQUERY_PORT}/" /app/data/ts/ts3server.ini

# update port in db
#mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "UPDATE servers SET server_port=${DEFAULT_PORT};"
if [ $(mysql -N -s --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} -e \
    "select count(*) from servers;") -ge 1 ]; then
    # update port
    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "UPDATE servers SET server_port=${DEFAULT_PORT};"
    echo "database found, updating port!"
else
    echo "nothing to do yet!"
fi

chown -R www-data.www-data /app/data /run/


echo "Starting Teamspeak Server!"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Teamspeak
