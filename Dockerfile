FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

MAINTAINER Authors name <support@cloudron.io>

ENV TSVERSION=3.7.1
ENV TS3SERVER_LICENSE=accept

EXPOSE 9987/udp
EXPOSE 30033
EXPOSE 10011

RUN apt-get update && apt-get install libmariadb3 && rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code/web /run/nginx/lib /run/nginx/logs /app/data/ts/files && \
    wget https://files.teamspeak-services.com/releases/server/${TSVERSION}/teamspeak3-server_linux_amd64-${TSVERSION}.tar.bz2 && \
    tar xfvj teamspeak3-server_linux_amd64-${TSVERSION}.tar.bz2 -C /app/code && \
    rm teamspeak3-server_linux_amd64-$TSVERSION.tar.bz2 && \
    rm -rf /var/lib/nginx && ln -s /run/nginx/lib /var/lib/nginx && \
    ln -s /app/data/ts/ts3db_mysql.ini /app/code/teamspeak3-server_linux_amd64/ts3db_mysql.ini && \
    ln -s /app/data/ts/files /app/code/teamspeak3-server_linux_amd64/files && \
    cp /app/code/teamspeak3-server_linux_amd64/redist/libmariadb.so.2 /app/code/teamspeak3-server_linux_amd64 && \
    chown -R cloudron.cloudron /app/code/teamspeak3-server_linux_amd64/

WORKDIR /app/code

# nginx stuff
RUN rm /etc/nginx/sites-enabled/* && \
    rm -rf /var/log/nginx && \
    ln -s /run/nginx/log /var/log/nginx && \
    chown -R www-data.www-data /app/data/ /run/

# copy index.html
COPY index.html /app/code/web

ADD start.sh /app/ 
ADD nginx.conf /etc/nginx/sites-enabled/

# Supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf


CMD [ "/app/start.sh" ]
